<?php

namespace Drupal\newsarticle;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a newsarticle entity type.
 */
interface NewsarticleInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the entity title.
   *
   * @return string
   *   Title of the entity.
   */
  public function getTitle();

  /**
   * Gets the entity creation time.
   *
   * @return string
   *   Creation time of the entity.
   */
  public function getCreatedTime();
}

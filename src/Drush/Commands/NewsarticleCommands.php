<?php

namespace Drupal\newsarticle\Drush\Commands;

use Drupal\user\Entity\User;
use Drupal\Core\Password\DefaultPasswordGenerator;
use Drupal\Component\Uuid\UuidInterface;
use Drush\Commands\DrushCommands;

/**
 * A drush command file
 * @package Drupal\newsarticle\Commands
 */

class NewsarticleCommands extends DrushCommands
{

  /**
   * Drush command that performs the task.
   * @command import-news
   * @usage newsarticle:import-news
   */

  public function importNews() {
    $connection = \Drupal::database();
    $url = "https://riad-news-api.vercel.app/api/news";
    $req = \Drupal::httpClient()->get($url);
    $res = $req->getBody();
    $res = json_decode($res, TRUE);
    foreach ($res['data'] as $r) {
      $uuid_service = \Drupal::service('uuid');
      $uuid = $uuid_service->generate();
      $userExists = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['name' => $r['source']]);
      if (!$userExists) {
        $user = User::create(['name' => $r['source'], 'pass' => DefaultPasswordGenerator::class]);
        $user->save();
      }
      $connection->insert('newsarticle')
        ->fields([
          'uuid' => $uuid,
          'title' => $r['title'],
          'status' => 1,
          'body__value' => $r['description'],
          'body__format' => 'plain',
          'author' => $user->id(),
          'created' => strtotime($r['pubDate'])
        ])
        ->execute();
    }
  }
}

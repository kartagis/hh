<?php

namespace Drupal\newsarticle\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the newsarticle entity edit forms.
 */
class NewsarticleForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New newsarticle %label has been created.', $message_arguments));
        $this->logger('newsarticle')->notice('Created new newsarticle %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The newsarticle %label has been updated.', $message_arguments));
        $this->logger('newsarticle')->notice('Updated newsarticle %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.newsarticle.canonical', ['newsarticle' => $entity->id()]);

    return $result;
  }

}

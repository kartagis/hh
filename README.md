# Important!
After cloning the module and enabling it, please don't forget to run `drush updb -y`. Of course, I can also make
modifications to the code.

I also built a permission based system, which was not among the requirements. I did it for fun.
